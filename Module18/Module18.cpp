#include <iostream>
using namespace std;

class Stack
{
public:

	struct Data
	{
		int x;
		Data* next;
	};

	void Push(int y)
	{
		Data* temp;
		temp = new Data;
		temp->x = y;
		if (top == NULL)
		{
			top = temp;
		}
		else
		{
			temp->next = top;
			top = temp;
		}

	}

	void Pop()
	{
		Data* temp = top;

		std::cout << temp->x << "\n";
		top = temp->next;
		delete temp;
	}

private:
	Data* top;

};

int main()
{
	Stack stek;
	stek.Push(100);
	stek.Push(200);
	stek.Push(350);
	stek.Pop();
	stek.Pop();
	stek.Pop();
}